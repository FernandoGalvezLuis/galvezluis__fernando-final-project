import React from 'react';

function Resume() {
    return (
        <div>
            <div className='containerRes'>
            <div className='ocupDate'>
                <h1 className='ocupation'>Solution Specialist Bilingual Spanish</h1>
                <h1 className='date'>Nov 2021- Present</h1>                
            </div>
            <p className="pointData company">Xperigo , Toronto, ON</p>
            <p className="pointData">Remotely locate customers in distress to set different services as to get them to safety </p>
            <p className="pointData">Accommodated customers with special necessities such as elderly population </p>
            <p className="pointData">Requested the necessary information and explained the service in a close but professional manner</p>
            <p className="pointData">Kept daily track of progress and managed communication with the rest of the team</p>
            <p className="pointData">Managed emergency situations due to extreme weather conditions and applied the appropriate protocols</p>
            <p className="pointData">Provided service for both the US and Canada</p>
            <br />
            <div className='ocupDate'>
                <h1 className='ocupation'>Teleperformance Canada Inc., Toronto, ON </h1>
                <h1 className='date'>Jun 2021-Aug 2021</h1>                
            </div>
            <p className="pointData company">One Contact Canada Inc., Toronto, ON </p>
            <p className="pointData">Provided excellent customer service to a foreign Bank, primarily Wells Fargo in English and Spanish</p>
            <p className="pointData">Provided customers with information according to their inquiries and assisted with the rewards points system</p>
            <p className="pointData">Managed customer accounts; Answered up to 80 calls per day</p>
            <p className="pointData">Updated customer information in system as required; Remained calm and focused on meeting customers’ needs in confrontational situations; Serviced other calls including high value customers</p>
            <p className="pointData">Trained, supervised, and assessed new employees at both English and Spanish service</p>
            <p className="pointData">Maintained and upheld confidentiality following security protocols by company’s standards and according to customer privacy act throughout all interactions; Cash handling experience</p>
            <br />
            <div className='ocupDate'>
                <h1 className='ocupation'>Food Market Researcher/Report Producer</h1>
                <h1 className='date'>Nov 2018-Jul 2019</h1>                
            </div>
            <p className="pointData company">AMBICAL.SA, A Coruña, Spain</p>
            <p className="pointData">On daily basis reported development and execution of the project, including the tests results of the individual projects; compiled statistical reports and translated the statistical data into untestable terms  </p>
            <p className="pointData">Reviewed and maintained the data base of participants for the purposes of the research, updated information when necessary; Recruited new participants, via phone calls</p>
            <p className="pointData">Organized the schedule of data gathering and sample testing day with participants </p>
            <p className="pointData">Managed the test samples by labeling products, recording test findings by individuals, and uploading results into data base for comparison</p>
            <p className="pointData">Worked on projects for high profile clients such as Centros Comerciales Carrefour, S.A</p>
            <p className="pointData">Set up equipment such as tablets to be used by participants; troubleshoot, ran a company software on each tablet to be used by participants </p>
            <p className="pointData">Created and helped implement new standards of conduct and guidelines for positions which were adopted throughout entire business</p>
            <p className="pointData">Managed and trained small groups of employees on a regular basis, participated in yearly employees’ performance reviews  </p>
            <p className="pointData">Supported the creation, population and quality assurance of client deliverables including the preparation of report charts; Monitored and reviewed the quality of data that’s collected to ensure sample and quotas are met</p>
            <br />
            <div className='ocupDate'>
                <h1 className='ocupation'>Data Recorder/Collector</h1>
                <h1 className='date'>Sept 2017-Nov 2017</h1>                
            </div>
            <p className="pointData company">National Institute of Statistics (Ministry of Economy, Spanish Government), Tenerife, Spain</p>
            <p className="pointData">Ensured the reliability and accuracy of the data collected, by following statistical rules and techniques</p>
            <p className="pointData">Collected data on site according to the instructions received by the Ministry of Economy</p>
            <p className="pointData">Incorporated data into official country index program</p>
            <p className="pointData">Reported development and execution daily to the assigned supervisor</p>
            <p className="pointData">Maintained clear and confidential contact with fellow employees and management</p>
            <br />
            <div className='ocupDate'>
                <h1 className='ocupation'>Group Manager of Statistical Data</h1>
                <h1 className='date'>Nov 2011-May 2012</h1>                
            </div>
            <p className="pointData company">National Institute of Statistics (Ministry of Economy, Spanish Government), Tenerife, Spain</p>
            <p className="pointData">Kept track of team progress while reporting new developments; ensured the team/individuals daily quotas are met for the 2011 Census</p>
            <p className="pointData">Collaborated with local authorities/government to ensure safe entry to selected streets and facilities</p>
            <p className="pointData">Compiled statistical reports, incorporating information about daily progress of the project; produced and maintained the data base of participants</p>
            <p className="pointData">Worked on the resolution of any issues or problems during the project; provided solutions in a timely manner</p>
            <p className="pointData">Assisted team members with any technical issues while on the field</p>
            <p className="pointData">Participated in weekly conferences with management teams, as a presenter, either at main office or traveling to secondary offices</p>
            <p className="pointData">Prepared weekly progress reports and coached new employees on best practices </p>
            <p className="pointData">Consulted and interacted with other departments and administrations to fully comply with official/government standards</p>
            <p className="pointData">Convened with and handled discourse for data processing at city level, while representing the Ministry of Economy</p>
            <br />
            </div>
            
        </div>
    )
}

export default Resume;