import React from 'react';

function ContactPage() {
    return (
        <div>
            <fieldset>
                <legend>Send me a message</legend>

            <form>
            <input id="name" className='name' type="text" placeholder="Full name" />
            <input id="Email" type="email" placeholder="YourEmailHere@site.com" />
            <textarea placeholder="Your message here"  rows="4" cols="50" id="textarea"></textarea>
            <input type="submit" className='submit' value="Send" />
            </form>

            </fieldset>
        </div>
    )
}

export default ContactPage;