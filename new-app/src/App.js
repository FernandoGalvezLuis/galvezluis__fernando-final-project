import logo from './logo.svg';
import './App.css';
import Home from './Home';
import Resume from './Resume';
import Portfolio from './Portfolio';
import ContactPage from './ContactPage';
import { Route, Link } from 'react-router-dom';
import NavBar from './NavBar';

function App() {
  return (
    <div className="App">
      <NavBar className="NavBar" />
      <Route exact path="/" component= {Home} />
      <Route exact path='/resume' component= {Resume} />
      <Route exact path='/portfolio' component= {Portfolio} />
      <Route exact path='/contactpage' component= {ContactPage} />
      
    </div>
  )
}

export default App;
