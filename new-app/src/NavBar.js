import React from 'react';
import {Link} from 'react-router-dom';

function NavBar() {
    return (
        <nav className='nav'>
            <p className='navItem'><Link to='/' className='linkText'>HOME</Link></p>
            <p className='navItem'><Link to='/Resume' className='linkText'>RESUME</Link></p>
            <p className='navItem'><Link to='/Portfolio' className='linkText'>PORTFOLIO</Link></p>
            <p className='navItem'><Link to='/ContactPage' className='linkText'>CONTACT PAGE</Link></p>
        </nav>
    )
}

export default NavBar;